import org.junit.Test;

import java.util.EmptyStackException;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.*;

public class StackListTest {

    @Test
    public void shouldPushAndPeakResult(){
        StackList<Integer> integerStackList = new StackList<>();
        integerStackList.push(10);
        final int result = integerStackList.peak();
        assertThat(result, is(10));
    }

    @Test(expected = EmptyStackException.class)
    public void shouldEmptyStackExceptionPopIfEmptyStack() {
        StackList<Integer> integerStackList = new StackList<>();
        integerStackList.pop();
    }

    @Test(expected = EmptyStackException.class)
    public void shouldEmptyStackExceptionPeakIfEmptyStack() {
        StackList<Integer> integerStackList = new StackList<>();
        integerStackList.peak();
    }

    @Test
    public void shouldEmptyResultTrue() {
        StackList<Integer> integerStackList = new StackList<>();
        final boolean result = integerStackList.empty();
        assertTrue(result);
    }

    @Test
    public void shouldEmptyResultFalse() {
        StackList<Integer> integerStackList = new StackList<>();
        integerStackList.push(1);
        final boolean result = integerStackList.empty();
        assertFalse(result);
    }
}
