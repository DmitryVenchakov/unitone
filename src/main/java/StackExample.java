import java.util.Stack;

public class StackExample {
    final static int MAX_VALUE = 100;
/*
    push -Ð´Ð¾Ð±Ð°Ð²Ð»ÑÐµÑ ÑÐ»ÐµÐ¼ÐµÐ½Ñ Ð² ÐºÐ¾Ð½ÐµÑ ÑÐ¿Ð¸ÑÐºÐ°
    peek - Ð²Ð¾Ð·Ð²ÑÐ°ÑÐ°ÐµÑ Ð¿Ð¾ÑÐ»ÐµÐ´Ð½Ð¸Ð¹ ÑÐ»ÐµÐ¼ÐµÐ½Ñ Ð¸ Ð¾ÑÐµÑÐµÐ´Ð¸, Ð½Ðµ ÑÐ´Ð°Ð»ÑÐµÑ ÐµÐ³Ð¾
    pop - ÑÐ°Ð»ÑÐµÑ Ð¿Ð¾ÑÐ»ÐµÐ´Ð½Ð¸Ð¹ Ð»ÐµÐ¼ÐµÐ½Ñ Ð¾ÑÐµÑÐµÐ´Ð¸ Ð¸ Ð²Ð¾Ð·Ð²ÑÐ°ÑÐ°ÐµÑ ÐµÐ³Ð¾
    empty - Ð²Ð¾Ð·Ð²ÑÐ°ÑÐ°ÐµÑ ÑÑÑ ÐµÑÐ»Ð¸ Ð¿ÑÑÑÐ¾Ð¹
    search - Ð²Ð¾Ð·Ð²ÑÐ°ÑÐ°ÐµÑ Ð½Ð¾Ð¼ÐµÑ Ð¿Ð¾Ð·Ð¸ÑÐ¸Ð¸ ÑÐ»ÐµÐ¼ÐµÐ½ÑÐ° Ñ ÐºÐ¾Ð½ÑÐ° Ð¾ÑÐµÑÐµÐ´Ð¸

 */
    public static void main(String[] args) {
        Stack<Integer> intStack = new Stack<Integer>();
        for (int i = 0; i < MAX_VALUE; i++) {
            intStack.push(i);
        }
        System.out.println(intStack);
        System.out.println("Ð£Ð´Ð°Ð»ÑÐµÐ¼ Ð¸Ð· ÑÑÐµÐºÐ° Ð´Ð¾ Ð¿ÑÑÑÐ¾Ð³Ð¾, ÑÐ´Ð°Ð»ÑÐµÐ¼ÑÐ¹ ÑÐ»ÐµÐ¼ÐµÐ½Ñ Ð¾ÑÐ¾Ð±ÑÐ°Ð¶Ð°ÐµÑÑÑ");

        while (!intStack.isEmpty()){
            System.out.println(intStack.pop());

            if(intStack.isEmpty()){
                System.out.println("ÐÑÐµÑÐµÐ´Ñ Ð¿ÑÑÑÐ°");
                System.out.println(intStack);
            }
        }
    }
}
