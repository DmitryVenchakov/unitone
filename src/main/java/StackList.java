import java.util.ArrayList;
import java.util.EmptyStackException;


public class StackList<T>{

    private int stackData = -1;
    private ArrayList<T> list = new ArrayList<T>();

    public void push(T i) {
        int j = ++stackData;
        list.add(j, i);
    }

    public synchronized T pop() {
        int index = list.size() - 1;
        if (index > -1) {
            T i = list.get(index);
            list.remove(index);
            return i;
        } else {
            throw new EmptyStackException();
        }
    }

    public synchronized T peak(){
        if (list.size() > 0) {
            return list.get(list.size() - 1);
        } else {
            throw new EmptyStackException();

        }
    }

    public boolean empty() {
        return list.size() < 1;
    }

    public static void main(String[] args){
        StackList<Integer> myList = new StackList<>();
        for(int i = 0; i < 2; i++)
        {
            myList.push(i);
        }
        System.out.println(myList.pop());
        System.out.println(myList.peak());

    }
}